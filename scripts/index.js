"use strict";

window.onload = init;

const orderDiv = document.getElementById("orderDiv");
orderDiv.style.display = "none";

function init() {
    const cone = document.getElementById("cone");
    const cup = document.getElementById("cup");
    const numberScoops = document.getElementById("numberScoops");

    numberScoops.onblur = onValidate;

    if (!cup.checked) {
        const toppings = document.getElementById("toppings");
        toppings.style.display = "none";
    }

    cone.onchange = toppings;
    cup.onchange = toppings;

    const orderBtn = document.getElementById("orderBtn");
    orderBtn.onclick = onOrderBtnClicked;
}

function onValidate() {
    const numberScoops = document.getElementById("numberScoops");
    if (numberScoops.value > 4) {
        alert('Too many scoops');
        numberScoops.focus = true;
    }
    if (numberScoops.value < 1) {
        alert('Too few scoops');
        numberScoops.focus = true;
    }

}

function toppings() {
    const toppings = document.getElementById("toppings");
    const sprinkles = document.getElementById("sprinkles");
    const hotFudge = document.getElementById("hotFudge");
    const whippedCream = document.getElementById("whippedCream");
    const cherry = document.getElementById("cherry");

    sprinkles.checked = false;
    hotFudge.checked = false;
    whippedCream.checked = false;
    cherry.checked = false;
    if (document.getElementById("cone").checked) {
        toppings.style.display = "none";
    }
    else {
        toppings.style.display = "block";
    }
}

function onOrderBtnClicked() {
    const basePrice = document.getElementById("basePrice");
    const basePriceSpan = document.getElementById("basePriceSpan");
    const tax = document.getElementById("tax");
    const taxSpan = document.getElementById("taxSpan");
    const totalDue = document.getElementById("totalDue");
    const totalDueSpan = document.getElementById("totalDueSpan");
    const numberScoops = document.getElementById("numberScoops");
    const sprinkles = document.getElementById("sprinkles");
    const hotFudge = document.getElementById("hotFudge");
    const whippedCream = document.getElementById("whippedCream");
    const cherry = document.getElementById("cherry");

    orderDiv.style.display = "block";

    let basePriceAmt = 2.25;
    let toppingsAmt = 0;
    if (numberScoops.value > 1) {
        basePriceAmt += ((numberScoops.value - 1) * 1.25);
    }
    if (sprinkles.checked) {
        basePriceAmt += .5;
    }
    if (hotFudge.checked) {
        basePriceAmt += 1.25;
    }
    if (whippedCream.checked) {
        basePriceAmt += .25;
    }
    if (cherry.checked) {
        basePriceAmt += .25;
    }
    basePrice.innerHTML = `Base price:`;
    basePriceSpan.innerHTML = `$${basePriceAmt.toFixed(2)}`;

    let taxAmt = basePriceAmt * .06;
    tax.innerHTML = `Tax (6%):`;
    taxSpan.innerHTML = `$${taxAmt.toFixed(2)}`;


    let totalDueAmt = basePriceAmt + taxAmt;
    totalDue.innerHTML = `Total (USD):`;
    totalDueSpan.innerHTML = `$${totalDueAmt.toFixed(2)}`;
}